FROM debian:9 as build
RUN apt update && apt install -y wget make gcc libpcre3-dev libssl-dev zlib1g-dev
RUN mkdir /tmp/nginx-build && cd /tmp/nginx-build
RUN wget https://openresty.org/download/nginx-1.19.3.tar.gz && wget http://luajit.org/download/LuaJIT-2.0.5.tar.gz && wget -O nginx_devel_kit.tar.gz https://github.com/simpl/ngx_devel_kit/archive/v0.3.1.tar.gz && wget -O nginx_lua_module.tar.gz https://github.com/openresty/lua-nginx-module/archive/v0.10.19.tar.gz
RUN tar xvf LuaJIT-2.0.5.tar.gz && tar xvf nginx-1.19.3.tar.gz && tar xvf nginx_devel_kit.tar.gz && tar xvf nginx_lua_module.tar.gz
RUN cd ./LuaJIT-2.0.5 && make install && ln -sf luajit-2.0.5 /usr/local/bin/luajit && cd ..
RUN cd ./nginx-1.19.3 && LUAJIT_LIB=/usr/local/lib LUAJIT_INC=/usr/local/include/luajit-2.0 && ./configure && make && make install

FROM debian:9 
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx 
CMD ["./nginx", "-g", "daemon off;"]

